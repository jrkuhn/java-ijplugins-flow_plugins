/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package temp;

/**
 *
 * @author jrkuhn
 */
public class RemainderTest {

    protected static void remainderTest() {
        for (int i = 0; i < 20; i++) {
            double number = 10 * (Math.random());
            if (i >= 10) {
                number = -number;
            }
            if (i >= 15) {
                number = (int)number;
            }
            //number = number + 10;
            int intpart = (int) number;
            if (intpart < 0) {
                intpart--;
            }
            double fraction = number - intpart;
            double remainder = number % 1;
            double IEEEremainder = Math.IEEEremainder(number, 1);
//            String line = "number = " + number;
//            line += "\t int = " + intpart;
//            line += "\t rem = " + remainder;
//            line += "\t IEEErem = " + IEEEremainder;
//            System.out.print(line+"\n");
            System.out.format("num: % 3.4f   int: %4d   frac: % 2.4f rem: % 2.4f   IEEErem: % 2.4f   1+IEEErem: % 2.4f\n",
                    number, intpart, fraction, remainder, IEEEremainder, 1 + IEEEremainder);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        remainderTest();

    }
}
