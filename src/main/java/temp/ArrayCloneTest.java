/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package temp;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jrkuhn
 */
public class ArrayCloneTest {

    static public class Foo {
        public int bar;
        public Foo(int bar) {
            this.bar = bar;
        }
        public Foo(Foo f) {
            this.bar = f.bar;
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Foo[] src = new Foo[10];
        for (int i=0; i<src.length; i++) {
            src[i] = new Foo(i);
        }

        // attempt to clone the array
        //Foo[] dest = src.clone();
        //Foo[] dest = new Foo[src.length];
        //System.arraycopy(src, 0, dest, 0, src.length);
        Foo[] dest = new Foo[src.length];
        for (int i=0; i<src.length; i++) {
            dest[i] = new Foo(src[i]);
        }

        // modify the source array
        for (int i=0; i<src.length; i++) {
            src[i].bar = src.length-i;
        }

        for (int i=0; i<src.length; i++) {
            System.out.format("[%2d]  src.bar: %2d   dest.bar: %2d\n", i, src[i].bar, dest[i].bar);
        }
    }

}
