/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package kuhnlab.plugins.flow;

import kuhnlab.coordinates.KPoint2D;

/** Used to hold correlation scores for a search area
 *
 * @author jrkuhn
 */
public class SearchPoint {
    public KPoint2D offset;     // offset from center for this search point
    public double radiusSq;     // distance^2 to the center.
    public double score;        // correlation score of this search point

    /** Creates a new search point at the corresponding offset. */
    public SearchPoint(double xoff, double yoff) {
        offset = new KPoint2D(xoff, yoff);
        radiusSq = offset.lengthSq();
        score = Double.NaN;
    }

    /** Copy constructor. */
    public SearchPoint(SearchPoint ref) {
        offset = (KPoint2D) ref.offset.clone();
        radiusSq = ref.radiusSq;
        score = ref.score;
    }

    /** Clear the score value by setting it to NaN. */
    public void clearScore() {
        score = Double.NaN;
    }

    /** Determine if the score is valid (not NaN). */
    public boolean hasScore() {
        return !Double.isNaN(score);
    }

}
