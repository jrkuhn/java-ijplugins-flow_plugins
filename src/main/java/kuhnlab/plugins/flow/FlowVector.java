/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package kuhnlab.plugins.flow;

import java.util.EnumSet;

/**
 *
 * @author jrkuhn
 */
public class FlowVector {
    public enum Quality {
        Unique, Significant, Reliable, Trackable
    };

    public int x;
    public int y;
    public double u;
    public double v;
    public EnumSet<Quality> quality;
    public int templateSize;

    public FlowVector() {
        this(0,0,0,0, EnumSet.noneOf(Quality.class), 0);
    }

    public FlowVector(int x, int y, double u, double v, EnumSet<Quality> quality, int templateSize) {
        this.x = x;
        this.y = y;
        this.u = u;
        this.v = v;
        this.quality = quality;
        this.templateSize = templateSize;
    }
}
