/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kuhnlab.plugins.flow;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import kuhnlab.coordinates.KPoint2D;
import kuhnlab.coordinates.KPolygon2D;
import kuhnlab.coordinates.Snake;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/** FlowField is used to store flow calculation data.
 *
 * @author jrkuhn
 */
@JsonIgnoreProperties({"protrusions", "retractions"})
public class FlowField {

    public List<FlowVector> flow;
    public Snake perimeter;
    public KPoint2D center;
    public double area;
    public double perimeterLength;
    public List<Snake> protrusions;
    public List<Snake> retractions;
    public double protrusionArea;
    public double retractionArea;
    // used to store flow calculations
    public double totalFlow;
    public int numFlows;
    public double totalAnterogradeFlow;
    public double totalLateralFlow;
    public double totalRetrogradeFlow;
    public int numAtereogradeFlows;
    public int numDirectedFlows;


    /** Default constructor. Used for JSON loading. */
    public FlowField() {
        flow = null;
        perimeter = null;
        center = null;
        protrusions = null;
        retractions = null;
    }

    public FlowField(Snake perimeter) {
        flow = new ArrayList<FlowVector>();
        this.perimeter = perimeter;
        center = perimeter.centroid();
        area = perimeter.polygonArea();
        perimeterLength += perimeter.getLength();
        protrusions = null;
        retractions = null;
    }

    public void calcPerimeterValues(Snake nextPerimeter) {
        if (perimeter != null) {
            center = perimeter.centroid();
            area = perimeter.polygonArea();
            perimeterLength = perimeter.getLength();
        } else {
            center = new KPoint2D(0,0);
            area = 0;
            perimeterLength = 0;
        }
        protrusionArea = 0;
        retractionArea = 0;

        if (nextPerimeter != null) {
            protrusions = nextPerimeter.subtract(perimeter);
            for (Snake s : protrusions) {
                protrusionArea += s.polygonArea();
            }
            retractions = perimeter.subtract(nextPerimeter);
            for (Snake s : retractions) {
                retractionArea += s.polygonArea();
            }
        }
    }

    public void addVector(int x, int y, double u, double v, EnumSet<FlowVector.Quality> quality, int templateSize) {
        flow.add(new FlowVector(x, y, u, v, quality, templateSize));
    }
}
