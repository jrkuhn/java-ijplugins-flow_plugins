/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kuhnlab.plugins.flow;

import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.GenericDialog;
import ij.gui.Roi;
import ij.io.OpenDialog;
import ij.io.SaveDialog;
import ij.measure.ResultsTable;
import ij.plugin.PlugIn;
import ij.plugin.filter.RankFilters;
import ij.process.AutoThresholder;
import ij.process.ByteProcessor;
import ij.process.FloatProcessor;
import ij.process.FloodFiller;
import ij.process.ImageProcessor;
import java.awt.geom.GeneralPath;

import javax.swing.*;
import java.awt.image.*;
import java.awt.*;
import java.io.File;
import javax.imageio.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import kuhnlab.coordinates.KPoint2D;
import kuhnlab.coordinates.KPolygon2D;
import kuhnlab.coordinates.Snake;
import kuhnlab.gui.overlay.OverlayRoi;
import kuhnlab.image.ImageCalculator;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author jrkuhn
 */
public class FlowTracker implements PlugIn /*, IJ.ExceptionHandler*/ {

    //################################################################
    //## THRESHOLD AND SNAKES PARAMETERS
    //##
    static protected RankFilters rankFilters = new RankFilters();
    // THRESHOLD PARAMETERS
    static final int DEFAULT_THRESHOLD_METHOD = AutoThresholder.Method.Li.ordinal();
    static String[] methodNames = AutoThresholder.getMethods();
    static String threshMethodString = methodNames[DEFAULT_THRESHOLD_METHOD];
    static boolean threshBlackBackground = true;
    static boolean threshGammaCorrect = true;
    static boolean threshMedianFilter = true;
    static boolean threshFill = true;
    static double threshGamma = 0.5;
    static double threshMedianRad = 2.0;
    static boolean threshShowOutlines = false;
    static boolean threshShowIntermediates = false;
    // GRADIENT VECTOR FLOW PARAMETERS
    static int gvfIterations = 80;
    static double gvfMu = 0.2;
    static boolean gvfNormalize = true;
    static boolean gvfShowQuivers = false;
    // SNAKE PARAMETES
    static int snakeMaxIterations = 100;
    static int snakeSubIterations = 4;
    static int snakeRefineIterations = 2;
    static double snakeStopCondition = 1e-5;
    static double snakeAlpha = 0.002;
    static double snakeBeta = 0.0002;
    static double snakeGamma = 1;
    static double snakeKappa = 0.5;
    static double snakeMinSpacing = 0.5;
    static double snakeMaxSpacing = 2.0;
    static boolean snakeShowIntermediates = false;
    static boolean snakeShowEvolution = false;
    //################################################################
    //## FLOW TRACKING PARAMETERS
    //##
    static int templateSizeMin = 7;  // minimum size (width,height) of the template
    static int templateSizeMax = 17; // maximum size (width,height) of the template
    static int timeWindow = 7;      // starting time template
    static double searchDiameterMin = 5;  // minimum size of the search window
    static double searchDiameterMax = 13; // maximum size of the search window
    static double searchStepMin = 0.25;   // minimum sub-pixel increments to shift template
    static double searchStepMax = 0.25;   // maximum sub-pixel increments to shift template
    static double significanceRadiusMax = 4.0;   // maximum significance radius. Depends on feature size
    static double significanceGamma = 0.5; // rejection criteria for local maxima
    static double epsilon = 1e-5; // tests for difference between two floating point numbers.
    static final double AUTOMATIC_TEMPLATE_SIZE_INC = 1.5;
    static final double TEMPLATE_SIZE_VARIATION = 1.25;
    static final double AUTOMATIC_SEARCH_STEP_INC = 2;
    static final double MAX_DISPLACEMENT_ERROR = 0.2; //0.2;    // 20% displacement error
    static final boolean FIND_CENTROID = true;   // use the cell centroid
    static final double ARROW_WIDTH = 0.3;
    static final double ARROW_HEIGHT = 0.3;
    static final boolean CIRCULAR_SEARCH_AREA = true; // use a circular search area
    static double arrowScale = 3.0;
    static int debugTotalFrames = -1;        // For diagnosis, only process a few frames
    static int flowSampleSpacing = 2;       // For diagnosis, skip some pixels
    static double drawTemplateProbability = 0.002; // 0.2% chance to draw template
    static boolean showEdgeDirection = false;    // show vectors from each point to nearest edge
    //################################################################
    //## STORAGE FOR COMPUTATION RESULTS
    //##
    static final String FLOW_DATA_PROPERTY = "FlowData";
    static final String FLOW_FILEDIR_PROPERTY = "FlowFileDir";
    static final String FLOW_FILENAME_PROPERTY = "FlowFileName";

    public boolean setupAutoSnakes() {
        GenericDialog gd = new GenericDialog("Auto Snake");

        gd.addMessage("------Auto Threshold Parameters------");
        gd.addChoice("Method", methodNames, threshMethodString);
        gd.addNumericField("Gamma Correct", threshGamma, 2);
        gd.addNumericField("Median Radius", threshMedianRad, 1);
        gd.addCheckbox("Black Background", threshBlackBackground);
        gd.addCheckbox("Gamma Correct", threshGammaCorrect);
        gd.addCheckbox("Median Filter", threshMedianFilter);
        gd.addCheckbox("Fill", threshFill);
        gd.addCheckbox("Show final outlines", threshShowOutlines);
        gd.addCheckbox("Show Intermediate Images", threshShowIntermediates);

        gd.addMessage("----Gradient Vector Flow Parameters---");
        gd.addNumericField("Iterations", gvfIterations, 0);
        gd.addNumericField("mu", gvfMu, 2);
        gd.addCheckbox("Normalize GVF vectors", gvfNormalize);
        gd.addCheckbox("Show GVF Quivers", gvfShowQuivers);
        //gd.addMessage("mu is the smoothing term:\nuse higher mu for noisy images.");

        gd.addMessage("-----------Snake Parameters-----------");
        gd.addNumericField("Max Iterations", snakeMaxIterations, 0);
        gd.addNumericField("Sub iterations", snakeSubIterations, 0);
        gd.addNumericField("Refine Iterations", snakeRefineIterations, 0);
        gd.addStringField("Termination delta", Double.toString(snakeStopCondition));
        gd.addNumericField("alpha", snakeAlpha, 4);
        gd.addNumericField("beta", snakeBeta, 4);
        gd.addNumericField("gamma", snakeGamma, 4);
        gd.addNumericField("kappa", snakeKappa, 4);
        gd.addNumericField("Min Point Spacing", snakeMinSpacing, 1);
        gd.addNumericField("Max Point Spacing", snakeMaxSpacing, 1);
        gd.addCheckbox("Show Intermediate Snakes", snakeShowIntermediates);
        gd.addCheckbox("Show Snake Evolution", snakeShowEvolution);

        URL url = getClass().getResource("flowhelp.html");
        if (url != null) {
            String strurl = url.toString();
            if (strurl.indexOf("://") < 0 && strurl.indexOf(":/") > 0) {
                strurl = strurl.replaceFirst(":/", ":///");
            }
            gd.addHelp(strurl);
        }
        gd.showDialog();
        if (gd.wasCanceled()) {
            return false;
        }

        threshMethodString = gd.getNextChoice();
        threshGamma = gd.getNextNumber();
        threshMedianRad = gd.getNextNumber();
        threshBlackBackground = gd.getNextBoolean();
        threshGammaCorrect = gd.getNextBoolean();
        threshMedianFilter = gd.getNextBoolean();
        threshFill = gd.getNextBoolean();
        threshShowOutlines = gd.getNextBoolean();
        threshShowIntermediates = gd.getNextBoolean();

        gvfIterations = (int) gd.getNextNumber();
        gvfMu = gd.getNextNumber();
        gvfNormalize = gd.getNextBoolean();
        gvfShowQuivers = gd.getNextBoolean();

        snakeMaxIterations = (int) gd.getNextNumber();
        snakeSubIterations = (int) gd.getNextNumber();
        snakeRefineIterations = (int) gd.getNextNumber();
        snakeStopCondition = Double.parseDouble(gd.getNextString());

        snakeAlpha = gd.getNextNumber();
        snakeBeta = gd.getNextNumber();
        snakeGamma = gd.getNextNumber();
        snakeKappa = gd.getNextNumber();
        snakeMinSpacing = gd.getNextNumber();
        snakeMaxSpacing = gd.getNextNumber();
        snakeShowIntermediates = gd.getNextBoolean();
        snakeShowEvolution = gd.getNextBoolean();

        return true;
    }

    public boolean setupFlowTracker() {
        GenericDialog gd = new GenericDialog("Flow Tracker");

        gd.addMessage("------Flow Computation Parameters------");
        gd.addNumericField("Min Template Size", templateSizeMin, 0);
        gd.addNumericField("Max Template Size", templateSizeMax, 0);
        gd.addNumericField("Time window", timeWindow, 0);
        gd.addNumericField("Min Search Diameter", searchDiameterMin, 0);
        gd.addNumericField("Max Search Diameter", searchDiameterMax, 0);
        gd.addNumericField("Min Search Step", searchStepMin, 2);
        gd.addNumericField("Max Search Step", searchStepMax, 2);
        gd.addNumericField("Significance Radius", significanceRadiusMax, 2);
        gd.addNumericField("Significance Gamma", significanceGamma, 2);
        gd.addMessage("---------Diagnostic Parameters--------");
        gd.addNumericField("Max frames to compute", debugTotalFrames, 0);
        gd.addNumericField("Sample spacing", flowSampleSpacing, 0);
        gd.addNumericField("Arrow scale", arrowScale, 2);
        gd.addNumericField("Templates to draw", drawTemplateProbability * 100, 1, 5, "%");
        gd.addCheckbox("Show edge directions", showEdgeDirection);

        URL url = getClass().getResource("flowhelp.html");
        if (url != null) {
            String strurl = url.toString();
            if (strurl.indexOf("://") < 0 && strurl.indexOf(":/") > 0) {
                strurl = strurl.replaceFirst(":/", ":///");
            }
            gd.addHelp(strurl);
        }
        gd.showDialog();
        if (gd.wasCanceled()) {
            return false;
        }

        templateSizeMin = (int) gd.getNextNumber();
        templateSizeMax = (int) gd.getNextNumber();
        timeWindow = (int) gd.getNextNumber();
        searchDiameterMin = gd.getNextNumber();
        searchDiameterMax = gd.getNextNumber();
        searchStepMin = gd.getNextNumber();
        searchStepMax = gd.getNextNumber();
        significanceRadiusMax = gd.getNextNumber();
        significanceGamma = gd.getNextNumber();

        debugTotalFrames = (int) gd.getNextNumber();
        flowSampleSpacing = (int) gd.getNextNumber();
        arrowScale = gd.getNextNumber();
        drawTemplateProbability = gd.getNextNumber() / 100.0;
        showEdgeDirection = gd.getNextBoolean();

        return true;
    }

    /** ImageJ Entry Point. */
    public void run(String arg) {
        ImagePlus imp = IJ.getImage();
        if (imp == null) {
            IJ.showMessage("No Image Found");
            return;
        }

        if (arg.equals("TestThreshold")) {
            // calculate thresholded outline
            ImagePlus impOutline = autoThreshold(imp, true);
            impOutline.show();
        } else if (arg.equals("TestSnakeSmoothing")) {
            Roi initialRoi = imp.getRoi();
            if (initialRoi == null) {
                IJ.showMessage("Please draw a circle around the cell first");
                return;
            }

            // calculate thresholded outline
            ImagePlus impOutline = autoThreshold(imp, true);
            if (threshShowOutlines) {
                impOutline.show();
            }
            // calculate snakes from outline
            Snake[] perimeters = autoSnake(impOutline, new Snake(initialRoi), imp);

            testSnakeSmoothing(imp, perimeters);

        } else if (arg.equals("ComputeFlow")) {
            Roi initialRoi = imp.getRoi();
            if (initialRoi == null || initialRoi instanceof OverlayRoi) {
                IJ.showMessage("Please draw a circle around the cell first");
                return;
            }

            if (!Template.CanHandleProcessor(imp.getProcessor())) {
                IJ.showMessage("Flow plugin cannot handle this image type.");
                return;
            }

            if (imp.getStackSize() < timeWindow) {
                IJ.showMessage("Need at least " + timeWindow + " frames to process.");
                return;
            }

            // calculate thresholded outline
            ImagePlus impOutline = autoThreshold(imp, true);
            if (threshShowOutlines) {
                impOutline.show();
            }
            // calculate snakes from outline
            Snake[] perimeters = autoSnake(impOutline, new Snake(initialRoi), imp);
            if (snakeShowEvolution) {
                showSnakeEvolution(imp, perimeters);
            }









        // BoundaryVelocity( perimeters );










            // Calculate the flows for this image
            FlowField[] flows = calculateFlow(imp, perimeters);

            // store the snakes and flows as "properties" of this image
            imp.setProperty(FLOW_DATA_PROPERTY, flows);

        } else if (arg.equals("SetupFlow")) {
            if (setupAutoSnakes()) {
                setupFlowTracker();
            }
        } else if (arg.equals("CreateOverlay")) {
            createOverlayedImage(imp);
        } else if (arg.equals("SaveFlow") || arg.equals("SaveFlowAs")) {
            FlowField[] flows = (FlowField[]) imp.getProperty(FLOW_DATA_PROPERTY);
            if (flows == null) {
                IJ.showMessage("Snakes and flows could not be found for this image");
                return;
            }

            boolean isSaveAs = arg.equals("SaveFlowAs");

            String dir = (String) imp.getProperty(FLOW_FILEDIR_PROPERTY);
            String name = (String) imp.getProperty(FLOW_FILENAME_PROPERTY);
            if (dir == null) {
                dir = "";
            }
            if (name == null) {
                name = "";
            }

            if (name.equals("")) {
                name = imp.getShortTitle();
                isSaveAs = true;
            }
            if (dir.equals("")) {
                isSaveAs = true;
            }

            if (isSaveAs) {
                IJ.wait(10);
                String ext = ".json";
                SaveDialog sd = new SaveDialog("Save Flow Data As", name, ext);
                name = sd.getFileName();
                if (name == null) {
                    return;
                }
                dir = sd.getDirectory();
            }
            String path = dir + name;

            if (saveFlowAs(path, flows)) {
                imp.setProperty(FLOW_FILENAME_PROPERTY, name);
            }
        } else if (arg.equals("OpenFlow")) {
            OpenDialog od = new OpenDialog("Open Text Image...", null);
            String dir = od.getDirectory();
            String name = od.getFileName();
            if (name == null) {
                return;
            }
            String path = dir + name;
            int nSlices = imp.getStackSize();

            IJ.showStatus("Reading Flow File");
            FlowField[] flows = OpenFlowAs(path, nSlices);

            if (flows != null) {
                updateProtrusionsAndRetractions(flows);
                imp.setProperty(FLOW_DATA_PROPERTY, flows);
                imp.setProperty(FLOW_FILEDIR_PROPERTY, dir);
                imp.setProperty(FLOW_FILENAME_PROPERTY, name);

                showAllFlows(imp, flows);

            } else {
                IJ.showMessage("Could not open flow field");
            }
        } else if (arg.equals("RedrawFlow")) {
            FlowField[] flows = (FlowField[]) imp.getProperty(FLOW_DATA_PROPERTY);
            if (flows == null) {
                IJ.showMessage("Could not find flow for image: " + imp.getTitle());
                return;
            }
            showAllFlows(imp, flows);
        } else if (arg.equals("ShowFlowMeasurements")) {
            FlowField[] flows = (FlowField[]) imp.getProperty(FLOW_DATA_PROPERTY);
            if (flows == null) {
                IJ.showMessage("Could not find flow for image: " + imp.getTitle());
                return;
            }
            showFlowResults(flows);
        }
    }


    public void BoundaryVelocity( Snake[] perimeters ) {

        double [] x_vect = new double [perimeters[0].points.size()];
        double [] y_vect = new double [perimeters[0].points.size()];
        double xmin,xmax,ymin,ymax;
        double xrange,yrange;
        double dx,dy;

        int xres,yres;
        int ii,jj,kk;

        for (ii=0;ii<perimeters[0].points.size();ii++) {
            x_vect[ii]=perimeters[0].points.get(ii).x;
            y_vect[ii]=perimeters[0].points.get(ii).y;
        }

        double [] D = new double [x_vect.length];

        xrange = array_max( x_vect ) - array_min( x_vect );
        yrange = array_max( y_vect ) - array_min( y_vect );

        xmin = array_min( x_vect )-0.25*xrange;
        xmax = array_max( x_vect )+0.25*xrange;
        ymin = array_min( y_vect )-0.25*yrange;
        ymax = array_max( y_vect )+0.25*yrange;

        xres=512;
        yres=512;

        double [][] X = new double [yres][xres];
        double [][] Y = new double [yres][xres];
        double [][] phi = new double [yres][xres];

        dx=(xmax-xmin)/(xres-1);
        dy=(ymax-ymin)/(yres-1);

        for (ii=0;ii<yres;ii++) {
            for (jj=0;jj<xres;jj++) {
                X[ii][jj]=xmin+dx*jj;
                Y[ii][jj]=ymin+dy*ii;
            }
        }

        double [] phi_image_double = new double [xres*yres];

        int n = 0;

        for (ii=0;ii<yres;ii++) {
            for (jj=0;jj<xres;jj++) {
                for (kk=0;kk<x_vect.length;kk++) {
                    D[kk] = Math.hypot(X[ii][jj]-x_vect[kk],Y[ii][jj]-y_vect[kk]);
                }
                phi[ii][jj] = array_min( D )*2*(inpolygon(x_vect,y_vect,X[ii][jj],Y[ii][jj])-0.5);
                phi_image_double[n] = phi[ii][jj];
                n++;
            }
        }


        double phi_min = array_min(phi_image_double);
        double phi_max = array_max(phi_image_double);
        int [] phi_image_int = new int [xres*yres];
        int phi_int;

        
        for (ii=0;ii<xres*yres;ii++) {
            phi_int = (int) Math.round(255*(phi_image_double[ii]-phi_min)/(phi_max-phi_min));
            phi_image_int[ii] = (phi_int << 16) | (phi_int << 8) | phi_int;
        }

        BufferedImage image;

        image = new BufferedImage(xres,yres,BufferedImage.TYPE_INT_RGB);
        image.setRGB(0,0,xres,yres,phi_image_int,0,xres);

        File filename_write = new File("/Users/rlafoy/Desktop/test.png");
        try {
            ImageIO.write(image,"png",filename_write);
        }
        catch(java.io.IOException ie) {}


        /*
        JFrame frame = new JFrame("ColorPan");
        frame.getContentPane().add(new ColorPan());
        frame.setSize(256,256);
        frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        frame.setVisible(true);
         */

        
        JFrame frame = new JFrame( "Snake Variable" );
        JLabel label = new JLabel( "Saved Image . . . ", JLabel.CENTER );
        frame.getContentPane().add(label);
        frame.setSize(300,300);
        frame.setVisible( true );
        
        
    }



    public double array_min( double [] x ) {
        // This function returns the minimum value of a double precision array

        double xmin;
        int ii;

        xmin=x[0];
        for (ii=0;ii<x.length;ii++) {
            if (x[ii]<xmin) {
                xmin=x[ii];
            }
        }

        return xmin;
    }

    public double array_max( double [] x ) {
        // This function returns the maximum value of a double precision array

        double xmax;
        int ii;

        xmax=x[0];
        for (ii=0;ii<x.length;ii++) {
            if (x[ii]>xmax) {
                xmax=x[ii];
            }
        }

        return xmax;
    }
    
    public double inpolygon( double [] x_vert, double [] y_vert, double x_test, double y_test ) {
        // This function tests whether the point (x_test,y_test) lies within the
        // polygon defined by the vectors x_vert and y_vert.  This uses the ray 
        // casting algorithm and is only appropriate for simple polygons.  The
        // first/last vertex may or may not be repeated.
        
        int ii,jj;
        int vert_num = x_vert.length;
        double ptn_test = 0;
        boolean bool_test_1,bool_test_2,bool_test_3;
        
        for (ii=0,jj=vert_num-1;ii<vert_num;jj=ii++) {
            
            bool_test_1 = y_vert[ii] > y_test;
            bool_test_2 = y_vert[jj] > y_test;
            bool_test_3 = x_test < (x_vert[jj]-x_vert[ii]) * (y_test-y_vert[ii]) / (y_vert[jj]-y_vert[ii]) + x_vert[ii];
            
            if ((bool_test_1 != bool_test_2) && bool_test_3) {
                ptn_test = 1 - ptn_test;
            }
        }
        
        return ptn_test;
        
    }

    /*
     int pnpoly(int nvert, float *vertx, float *verty, float testx, float testy)
{
  int i, j, c = 0;
  for (i = 0, j = nvert-1; i < nvert; j = i++) {
    if ( ((verty[i]>testy) != (verty[j]>testy)) &&
	 (testx < (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]) )
       c = !c;
  }
  return c;
}

     */


    //#######################################################################
    //## SNAKES AND THRESHOLD STUFF
    //#######################################################################
    public ImagePlus autoThreshold(ImagePlus impSrc, boolean asOutline) {
        if (impSrc.getProcessor() instanceof FloatProcessor) {
            // TODO: Support float processor
            return null;
        }

        int width = impSrc.getWidth();
        int height = impSrc.getHeight();
        int nSlices = impSrc.getNSlices();

        ImageStack stackGamma = null;
        ImageStack stackMedian = null;
        ImageStack stackThreshold = null;
        ImageStack stackFilled = null;
        ImageStack stackFinal = new ImageStack(width, height);

        if (threshShowIntermediates) {
            stackGamma = new ImageStack(width, height);
            stackMedian = new ImageStack(width, height);
            stackThreshold = new ImageStack(width, height);
            stackFilled = new ImageStack(width, height);
        }

        ImageStack stackSrc = impSrc.getImageStack();
        String label;
        String type = asOutline ? "outline" : "final threshold";

        for (int slice = 1; slice <= nSlices; slice++) {
            IJ.showStatus("Outline");
            IJ.showProgress(slice, nSlices);
            ImageProcessor ipThresh = stackSrc.getProcessor(slice);
            label = stackSrc.getSliceLabel(slice);
            // work with a copy
            ipThresh = ipThresh.duplicate();

            if (threshGammaCorrect) {
                ipThresh.gamma(threshGamma);
                if (stackGamma != null) {
                    stackGamma.addSlice(label + " gamma", ipThresh.duplicate());
                }
            }

            if (threshMedianFilter) {
                rankFilters.rank(ipThresh, threshMedianRad, RankFilters.MEDIAN);
                if (stackMedian != null) {
                    stackMedian.addSlice(label + " median", ipThresh.duplicate());
                }
            }

            // convert the auto threshold string to a method
            // and apply the auto threshold to the image
            AutoThresholder.Method method = AutoThresholder.Method.valueOf(AutoThresholder.Method.class, threshMethodString);
            method = (method == null) ? method = AutoThresholder.Method.Default : method;
            ipThresh.setAutoThreshold(method, threshBlackBackground);
            if (ipThresh instanceof FloatProcessor) {
                // TODO: Add thresholding to FloatProcessor
                return null;
            } else {
                ipThresh.autoThreshold();
            }

            ByteProcessor bp = (ByteProcessor) ipThresh.convertToByte(false);
            if (stackThreshold != null) {
                stackThreshold.addSlice(label + " initial threshold", bp.duplicate());
            }

            if (threshFill) {
                fillBinary(bp, 255, 0);
                if (stackFilled != null) {
                    stackFilled.addSlice(label + " filled", bp.duplicate());
                }
            }

            if (asOutline) {
                bp.outline();
                bp.invert();
            }
            stackFinal.addSlice(label + " " + type, bp);
        }

        label = impSrc.getShortTitle();
        if (stackGamma != null) {
            (new ImagePlus(label + " gamma", stackGamma)).show();
        }
        if (stackMedian != null) {
            (new ImagePlus(label + " median", stackMedian)).show();
        }
        if (stackThreshold != null) {
            (new ImagePlus(label + " initial threshold", stackThreshold)).show();
        }
        if (stackFilled != null) {
            (new ImagePlus(label + " filled", stackFilled)).show();
        }

        IJ.showStatus("");
        IJ.showProgress(1.0);

        return new ImagePlus(label + " " + type, stackFinal);
    }

    public ImagePlus edges(ImagePlus impSrc, boolean squareFirst) {
        int width = impSrc.getWidth();
        int height = impSrc.getHeight();
        int nSlices = impSrc.getNSlices();

        String label;

        ImageStack stackSrc = impSrc.getImageStack();
        ImageStack stackDest = new ImageStack(width, height);

        for (int slice = 1; slice <= nSlices; slice++) {
            FloatProcessor fpDest = (FloatProcessor) stackSrc.getProcessor(slice).convertToFloat();
            label = stackSrc.getSliceLabel(slice);

            if (squareFirst) {
                fpDest.sqr();
            }

            if (true) {
                fpDest.gamma(threshGamma);
            }
            fpDest.findEdges();
            stackDest.addSlice(label + " edges", fpDest);
        }

        label = impSrc.getShortTitle();
        return new ImagePlus(label + " edges", stackDest);
    }

    public Snake[] autoSnake(ImagePlus impOutline, Snake lastSnake, ImagePlus impOriginal) {
        final boolean SHOW_CONTROL_POINTS = true;

        int width = impOutline.getWidth();
        int height = impOutline.getHeight();
        int nSlices = impOutline.getNSlices();

        if (lastSnake == null) {
            return null;
        }

        ImageStack stackEdge = null;
        if (snakeRefineIterations > 0) {
            stackEdge = this.edges(impOriginal, false).getStack();
            if (this.snakeShowIntermediates) {
                (new ImagePlus("Edges", stackEdge)).show();
            }
        }

        ImageStack stackFilled = null;  // new ImageStack(width, height);
        ImageStack stackMask = null;    // new ImageStack(width, height);

        OverlayRoi overlayOutline = null; //new OverlayRoi(impOutline);
        OverlayRoi overlay = (impOriginal == null) ? null : new OverlayRoi(impOriginal, false);
        final BasicStroke thinStroke = new BasicStroke(0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER);
        final BasicStroke thickStroke = new BasicStroke(0.8f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER);

        Snake[] snakes = new Snake[nSlices];
        ImageStack stackOutline = impOutline.getImageStack();

        int nLastFrame = nSlices;
        if (debugTotalFrames > 0) {
            nLastFrame = debugTotalFrames + 1;
        }

        for (int slice = 1; slice <= nLastFrame; slice++) {
            IJ.showStatus("Snake " + slice + "/" + nLastFrame);
            FloatProcessor fpSrc = (FloatProcessor) stackOutline.getProcessor(slice).convertToFloat();
            FloatProcessor fpU = new FloatProcessor(width, height);
            FloatProcessor fpV = new FloatProcessor(width, height);

            // Calculate the Gradient Vector Flow values
            ImageCalculator.gradientVectorFlow(fpSrc, fpU, fpV, gvfMu, gvfIterations);

            // Normalize the GVF vectors
            if (gvfNormalize) {
                ImageCalculator.normalizeGradientVectorFlow(fpU, fpV, 1.0E-6);
            }

            if (gvfShowQuivers && overlay != null) {
                // show quivers on the original image
                Color col = Color.GRAY;
                for (int y = 0; y < height; y++) {
                    for (int x = 0; x < width; x++) {
                        if (x == 21 && y == 21) {
                            int dummy = 0;
                        }
                        KPoint2D ptCenter = new KPoint2D(x, y);
                        KPoint2D vDir = new KPoint2D(fpU.getPixelValue(x, y), fpV.getPixelValue(x, y));
                        //vDir.scale(1.0);
                        KPoint2D ptEnd = KPoint2D.newSumOf(ptCenter, vDir);
                        overlay.addShape(slice, FlowTracker.createArrow(ptCenter, ptEnd, 0.4, 0.4), thinStroke, col, null);
                    }
                }
            }

            FloatProcessor fpURefine = null, fpVRefine = null;
            if (snakeRefineIterations > 0) {
                fpURefine = new FloatProcessor(width, height);
                fpVRefine = new FloatProcessor(width, height);
                FloatProcessor fpEdge = (FloatProcessor) stackEdge.getProcessor(slice).convertToFloat();

                // Calculate the Gradient Vector Flow values
                ImageCalculator.gradientVectorFlow(fpEdge, fpURefine, fpVRefine, gvfMu, gvfIterations);

                // Normalize the GVF vectors
//                if (gvfNormalize) {
//                    ImageCalculator.normalizeGradientVectorFlow(fpU, fpV);
//                }
            }

            // Now use the lastSnake to create the next points
            Snake snake = new Snake(lastSnake);
            if (overlay != null) {
                overlay.addShape(slice, snake.toGeneralPath(), thinStroke, Color.RED, null);
            }

            Snake.EvolveResults res;
            int iter = 0;
            do {
                IJ.showProgress(iter, snakeMaxIterations);
                snake.resample(snakeMaxSpacing);
                res = snake.evolve(snakeAlpha, snakeBeta, snakeGamma, snakeKappa, fpU, fpV, snakeSubIterations);
                snake.removeDuplicates(snakeMinSpacing);
//                IJ.write("Iteration: " +iter+ "\t N: " + points.points.size() + "\t maxDist: " +IJ.d2s(res.maxDist,3)
//                        + "\t avgDist: " +IJ.d2s(res.avgDist,3)+ "\t minDist: " +IJ.d2s(res.minDist,10));

                if (snakeShowIntermediates && overlay != null) {
                    overlay.addShape(slice, snake.toGeneralPath(), thinStroke, Color.RED.darker(), null);
                    if (SHOW_CONTROL_POINTS) {
                        for (KPoint2D pt : snake.points) {
                            overlay.addShape(slice, pt.toRectangleShape(0.5, 0.5), null, null, Color.RED);
                        }
                    }
                }
                iter++;
            } while (res.minDist > snakeStopCondition && iter < snakeMaxIterations);

            if (snakeRefineIterations > 0) {
                iter = 0;
                do {
                    IJ.showProgress(iter, snakeRefineIterations);
                    snake.resample(snakeMaxSpacing);
                    res = snake.evolve(snakeAlpha, snakeBeta, snakeGamma, snakeKappa, fpURefine, fpVRefine, snakeSubIterations);
                    snake.clipToRect(0, 0, width, height);
                    snake.removeDuplicates(snakeMinSpacing);
                    if (snakeShowIntermediates && overlayOutline != null) {
                        overlayOutline.addShape(slice, snake.toGeneralPath(), thinStroke, Color.BLUE.darker(), null);
                        if (SHOW_CONTROL_POINTS) {
                            for (KPoint2D pt : snake.points) {
                                overlayOutline.addShape(slice, pt.toRectangleShape(0.5, 0.5), null, null, Color.BLUE);
                            }
                        }
                    }
                    iter++;
                } while (iter < snakeRefineIterations);
            }

            snake.resample(snakeMaxSpacing);
            lastSnake = snake;
            snakes[slice - 1] = snake;

            if (overlayOutline != null) {
                overlayOutline.addShape(slice, snake.toGeneralPath(), thickStroke, Color.YELLOW.darker(), null);
                if (SHOW_CONTROL_POINTS) {
                    for (KPoint2D pt : snake.points) {
                        overlayOutline.addShape(slice, pt.toRectangleShape(0.7, 0.7), null, null, Color.YELLOW);
                    }
                }

            }
            if (overlay != null) {
                overlay.addShape(slice, snake.toGeneralPath(), thinStroke, Color.RED, null);
            }


            if (stackFilled != null) {
                // Draw the filled version
                ByteProcessor bp = snake.toMask(width, height, true);
                stackFilled.addSlice("", bp);
            }

            if (stackMask != null) {
                // Draw the filled version
                ByteProcessor bp = snake.toMask(width, height, false);
                stackMask.addSlice("", bp);
            }

            IJ.showProgress(1.0);
        }
        IJ.showStatus("");

        if (overlay != null && impOriginal != null) {
            impOriginal.setRoi(overlay);
            impOriginal.updateAndDraw();
        }

        if (overlayOutline != null) {
            impOutline.setRoi(overlayOutline);
            impOutline.updateAndDraw();
        }

        if (stackFilled != null) {
            (new ImagePlus("Filled snake", stackFilled)).show();
        }

        if (stackMask != null) {
            (new ImagePlus("Masked snake", stackMask)).show();
        }

        return snakes;
    }

    // Binary fill by Gabriel Landini, G.Landini at bham.ac.uk
    // 21/May/2008
    // COPIED FROM ImageJ class ij.plugin.filter.Binary
    void fillBinary(ImageProcessor ip, int foreground, int background) {
        int width = ip.getWidth();
        int height = ip.getHeight();
        FloodFiller ff = new FloodFiller(ip);
        ip.setColor(127);
        for (int y = 0; y < height; y++) {
            if (ip.getPixel(0, y) == background) {
                ff.fill(0, y);
            }
            if (ip.getPixel(width - 1, y) == background) {
                ff.fill(width - 1, y);
            }
        }
        for (int x = 0; x < width; x++) {
            if (ip.getPixel(x, 0) == background) {
                ff.fill(x, 0);
            }
            if (ip.getPixel(x, height - 1) == background) {
                ff.fill(x, height - 1);
            }
        }
        byte[] pixels = (byte[]) ip.getPixels();
        int n = width * height;
        for (int i = 0; i < n; i++) {
            if (pixels[i] == 127) {
                pixels[i] = (byte) background;
            } else {
                pixels[i] = (byte) foreground;
            }
        }
    }

    protected void showSnakeEvolution(ImagePlus imp, Snake[] snakes) {
        int width = imp.getWidth();
        int height = imp.getHeight();

        int slice = 1;
        imp.setSlice(slice);
        ImagePlus impFirst = new ImagePlus("Snakes", imp.getProcessor());

        OverlayRoi overlay = new OverlayRoi(impFirst, true);
        final BasicStroke thinStroke = new BasicStroke(0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER);

        int nSnakes = snakes.length;
        for (int i = 0; i < nSnakes; i++) {
            Snake snake = snakes[i];
            if (snake != null) {
                float hue = ((float) (i + 1)) / nSnakes;
                Color col = new Color(Color.HSBtoRGB((float) (0.2 + 0.8 * hue), 1f, 1f));
                overlay.addShape(slice, snake.toGeneralPath(), thinStroke, col, null);
            }
        }

        impFirst.setRoi(overlay);
        impFirst.show();
    }

    //#######################################################################
    //## FLOW STUFF
    //#######################################################################
    void gatherMultiFramePixels(ImageStack stack, Template template, int firstFrame, int lastFrame, float[] pixels) {
        int templateSize = template.numPoints();
        int start = 0;
        ImageProcessor ip;
        for (int frame = firstFrame; frame <= lastFrame; frame++) {
            ip = stack.getProcessor(frame);
            template.gatherPixels(ip, pixels, start, templateSize);
            start += templateSize;
        }
    }

    private boolean saveFlowAs(String path, FlowField[] flows) {

        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonFactory factory = new JsonFactory();
            Writer writer = new FileWriter(path);
            JsonGenerator generator = factory.createJsonGenerator(writer);
            generator.useDefaultPrettyPrinter();
            mapper.writeValue(generator, flows);
        } catch (IOException ex) {
            IJ.showMessage("WARNING: Could not save flow file: " + ex);
            return false;
        }
        return true;
    }

    private FlowField[] OpenFlowAs(String path, int maxFrames) {
        FlowField[] flows = null;
        try {
            Reader reader = new FileReader(path);
            ObjectMapper mapper = new ObjectMapper();
            flows = mapper.readValue(reader, FlowField[].class);
            //reader.doublePoints();
        } catch (FileNotFoundException ex) {
            IJ.showMessage("ERROR: Could not read flow file: " + ex);
            return null;
        } catch (IOException ex) {
            IJ.showMessage("ERROR: Could not read flow file: " + ex);
            return null;
        }

        return flows;
    }

    private void testSnakeSmoothing(ImagePlus imp, Snake[] perimeters) {
        int imageWidth = imp.getWidth();
        int imageHeight = imp.getHeight();
        int nSlices = imp.getStackSize();

        final BasicStroke thinStroke = new BasicStroke(0.2f);
        final BasicStroke hairStroke = new BasicStroke(0.05f);
        OverlayRoi overlay = new OverlayRoi(imp, true);

        for (int iSlice=0; iSlice<nSlices; iSlice++) {
            if (perimeters[iSlice] == null) {
                continue;
            }
            int frame = iSlice+1;
            KPolygon2D perimeter;
            // force an odd number of points in the original perimeter
            perimeter = perimeters[iSlice].fourierDownsampledCopy(1.0);
            KPolygon2D fd = perimeter.fourierDescriptors(true);
            overlay.addShape(frame, perimeter.toGeneralPath(), thinStroke, Color.YELLOW, null);
            // Coefficient z[0] is the x,y position of the center
            // Coefficients z[-1],z[+1] define the fit ellipse of the pattern
            // The length of the minor axis of the ellipse is |z[+1]| - |z[-1]|
            // The length of the major axis of the ellipse is |z[+1]| + |z[-1]|
            // The angle of the minor axis is arg(z[+1]) - arg(z[-1])
            KPoint2D z0 = fd.points.get(0);
            KPoint2D zp1 = fd.points.get(1);
            KPoint2D zm1 = fd.points.get(fd.points.size()-1);
            double zp1MAG = zp1.length();
            double zp1ARG = zp1.angle();
            double zm1MAG = zm1.length();
            double zm1ARG = zm1.angle();
            double aLen = 2*(zp1MAG - zm1MAG);
            double bLen = 2*(zp1MAG + zm1MAG);
            double aAngle = (zp1ARG - zm1ARG);
            double bAngle = aAngle + Math.PI / 2;

            double avgLen = (aLen + bLen)/2;
            overlay.addShape(frame, z0.toCircleShape(avgLen/2), hairStroke, Color.GREEN, null);

            IJ.write("a="+IJ.d2s(aLen)+"   b="+IJ.d2s(bLen)+"   avg="+IJ.d2s(avgLen));

//            KPoint2D pt;
//            pt = KPoint2D.newPolar(aLen/2, aAngle);
//            pt.add(z0);
//            //overlay.addShape(frame, pt.toCircleShape(1.5), null, null, Color.GREEN);
//            overlay.addString(frame, pt.x, pt.y, "a1", null, null, Color.GREEN);
//            KPoint2D pa1 = pt;
//
//            pt = KPoint2D.newPolar(bLen/2, bAngle);
//            pt.add(z0);
//            //overlay.addShape(frame, pt.toCircleShape(1.5), null, null, Color.GREEN);
//            overlay.addString(frame, pt.x, pt.y, "b1", null, null, Color.GREEN);
//            KPoint2D pb1 = pt;
//
//            pt = KPoint2D.newPolar(aLen/2, aAngle + Math.PI);
//            pt.add(z0);
//            //overlay.addShape(frame, pt.toCircleShape(1.5), null, null, Color.GREEN);
//            overlay.addString(frame, pt.x, pt.y, "a2", null, null, Color.GREEN);
//            KPoint2D pa2 = pt;
//
//            pt = KPoint2D.newPolar(bLen/2, bAngle + Math.PI);
//            pt.add(z0);
//            //overlay.addShape(frame, pt.toCircleShape(1.5), null, null, Color.GREEN);
//            overlay.addString(frame, pt.x, pt.y, "b2", null, null, Color.GREEN);
//            KPoint2D pb2 = pt;
//
//            KPolygon2D aPoly = new KPolygon2D(2, false);
//            aPoly.points.add(pa1);
//            aPoly.points.add(pa2);
//
//            KPolygon2D bPoly = new KPolygon2D(2, false);
//            bPoly.points.add(pb1);
//            bPoly.points.add(pb2);
//
//            overlay.addShape(frame, aPoly.toGeneralPath(), hairStroke, Color.CYAN, null);
//            overlay.addShape(frame, bPoly.toGeneralPath(), hairStroke, Color.CYAN, null);
//
//            Shape ellipse = new Ellipse2D.Double(-aLen/2, -bLen/2, aLen, bLen);
//            AffineTransform trans = new AffineTransform();
//            trans.translate(z0.x, z0.y);
//            trans.rotate(aAngle);
//            overlay.addShape(frame, trans.createTransformedShape(ellipse), hairStroke, Color.MAGENTA, null);

            for (int i=-10; i<=10; i++) {
                int index = i < 0 ? fd.points.size() + i : i;
                KPoint2D p = fd.points.get(index);
                IJ.write("FD["+i+"] \tx="+IJ.d2s(p.x)+" \ty="+IJ.d2s(p.y)+" \tMAG="+IJ.d2s(p.length())+" \tARG="+IJ.d2s(Math.toDegrees(p.angle())));
            }
            KPolygon2D nextPerim;
            for (int i=0; i<=1; i++) {
                double fraction = 1.0 - 0.8*((double)i)/10.0;
                nextPerim = perimeter.fourierSmoothedCopy(i, 1.0, true);
                KPoint2D ptText = nextPerim.points.get(0+i*6);
                if (i==0) {
                    overlay.addShape(frame, nextPerim.points.get(0).toCircleShape(1.0), null, null, Color.YELLOW);
                } else {
                    overlay.addShape(frame, nextPerim.toGeneralPath(), thinStroke, Color.YELLOW, null);
                    overlay.addString(frame, ptText.x, ptText.y, ""+i, thinStroke, null, Color.WHITE);
                }
            }
            int nSteps = (int)(avgLen/2);
            int nWidthCortex = nSteps;//28;
            int nWidthBody = nSteps - nWidthCortex;
            int nMaxSamplePairs = perimeter.points.size()/2;
            for (int iStep=nSteps; iStep>0; iStep-=1) {
                int iCortex = iStep - nWidthBody;
                if (iCortex < 0)
                    iCortex = 0;
                double fractionCortex = ((double)iCortex)/nWidthCortex;
                int nSamplePairs = (int)(nMaxSamplePairs*Math.pow(fractionCortex,1)) + 1;
                double fraction = ((double)iStep)/nSteps;
                IJ.write("iStep="+iStep+"   nPairs="+nSamplePairs+"   fraction="+fraction);
                //nextPerim = perimeter.fourierSmoothedCopy(Math.pow(fraction, 4), fraction);
                nextPerim = perimeter.fourierSmoothedCopy(Math.pow(1.0, 1), fraction, true);
                overlay.addShape(frame, nextPerim.toGeneralPath(), hairStroke, Color.RED, null);
                overlay.addShapes(frame, nextPerim.toVertices(0.5), null, null, Color.RED);
            }
        }
        imp.setRoi(overlay);
        imp.updateAndRepaintWindow();
    }

    protected static class CorrelationData {

        public Template template;
        public SearchPoint globalMax;
        public double displacementOfMax;
        public int xIndexOfMax, yIndexOfMax;
        public boolean isUnique, isSignificant, isReliable, isTrackable;

        public void clear() {
            template = null;
            globalMax = null;
            displacementOfMax = Double.NaN;
            xIndexOfMax = yIndexOfMax = 0;
            isUnique = isSignificant = isReliable = isTrackable = false;
        }
    }

    public void updateProtrusionsAndRetractions(FlowField[] flowFields) {
        for (int i = 0, j = 1; i < flowFields.length - 1; i++, j++) {
            Snake nextPerimeter = null;
            if (flowFields[j] != null) {
                nextPerimeter = flowFields[j].perimeter;
            }
            if (flowFields[i] != null) {
                if (flowFields[i].perimeter != null) {
                    flowFields[i].calcPerimeterValues(nextPerimeter);
                }
            }
        }
    }

    public void showAllFlows(ImagePlus imp, FlowField[] flowFields) {
        int width = imp.getWidth();
        int height = imp.getHeight();
        int nSlices = imp.getStackSize();

        OverlayRoi overlay = new OverlayRoi(imp, true);
        imp.setRoi(overlay);

        int nPixels = width*height;
        int nDiagPoints = (int)(nPixels * drawTemplateProbability);
        List<KPoint2D> diagPoints = new ArrayList<KPoint2D>(nDiagPoints);

        Random rand = new Random();
        int xSteps = width / flowSampleSpacing;
        int ySteps = height / flowSampleSpacing;
        for (int i=0; i<nDiagPoints; i++) {
            int x = rand.nextInt(xSteps) * flowSampleSpacing;
            int y = rand.nextInt(ySteps) * flowSampleSpacing;
            diagPoints.add(new KPoint2D(x,y));
        }

        IJ.showStatus("Showing Flow Map");
        for (int i = 0; i < nSlices; i++) {
            int frame = i + 1;
            double progress = frame;
            progress /= nSlices;
            IJ.showProgress(progress);
            FlowField flowField = flowFields[i];
            if (flowField != null) {
                ByteProcessor mask = null;
                if (flowField.perimeter != null) {
                    mask = flowField.perimeter.toMask(width, height, true);
                }
                overlayAndSumFlow(overlay, frame, flowField, mask, diagPoints);
            }
        }
        imp.setRoi(overlay);
        imp.updateAndDraw();
        IJ.showProgress(1.0);
        IJ.showStatus("");
    }

    public void overlayAndSumFlow(OverlayRoi overlay, int frame, FlowField flowField, ByteProcessor mask, List<KPoint2D> diagPoints) {
        final KPoint2D ptHALF_PIXEL = new KPoint2D(0.5, 0.5);
        final Color colorProt = new Color(255, 0, 0, 64);
        final Color colorRetr = new Color(0, 255, 0, 64);
        final Color colorNearestEdge = new Color(64, 64, 64, 0);
        final BasicStroke thinStroke = new BasicStroke(0.2f);
        final BasicStroke hairStroke = new BasicStroke(0.05f);

        // draw the perimeters, including protrusions and retractions
        if (flowField.perimeter != null) {
            if (flowField.protrusions != null) {
                for (Snake s : flowField.protrusions) {
                    overlay.addShape(frame, s.offset(ptHALF_PIXEL).toGeneralPath(), null, null, colorProt);
                }
            }
            if (flowField.retractions != null) {
                for (Snake s : flowField.retractions) {
                    overlay.addShape(frame, s.offset(ptHALF_PIXEL).toGeneralPath(), null, null, colorRetr);
                }
            }
            overlay.addShape(frame, flowField.perimeter.offset(ptHALF_PIXEL).toGeneralPath(), thinStroke, Color.YELLOW, null);
        }

        flowField.numFlows = 0;
        flowField.numAtereogradeFlows = 0;
        flowField.numDirectedFlows = 0;
        flowField.totalFlow = 0;
        flowField.totalAnterogradeFlow = 0;
        flowField.totalRetrogradeFlow = 0;
        flowField.totalLateralFlow = 0;

        for (FlowVector flowPoint : flowField.flow) {
            KPoint2D ptCurrent = new KPoint2D(flowPoint.x, flowPoint.y);

            boolean isDiagPoint = false;
            for (KPoint2D p : diagPoints) {
                if (p.epsilonEquals(ptCurrent, 0.01)) {
                    isDiagPoint = true;
                    break;
                }
            }

            KPoint2D ptCurrentOFFSET = KPoint2D.newSumOf(ptCurrent, ptHALF_PIXEL);
            KPoint2D vDir = new KPoint2D(flowPoint.u, flowPoint.v);
            flowField.totalFlow += vDir.length();
            flowField.numFlows++;

            KPoint2D vExtDir = KPoint2D.newProdOf(arrowScale, vDir);
            KPoint2D ptEnd = KPoint2D.newSumOf(ptCurrentOFFSET, vExtDir);
            Color shiftColor = Color.DARK_GRAY;
            GeneralPath quiverPath = createArrow(ptCurrentOFFSET, ptEnd, ARROW_WIDTH, ARROW_HEIGHT);
            GeneralPath edgePath = null;

            // Determine if we are inside or on the boundary of the points outline
            int maskVal = mask.get(flowPoint.x, flowPoint.y);
            boolean isInsideSnake;
            if (maskVal == 255) {
                // we are definitely inside the points
                isInsideSnake = true;
            } else if (maskVal > 0) {
                // we are on the points boundary. Test for point in polygon
                isInsideSnake = flowField.perimeter.pointInPolygon(flowPoint.x, flowPoint.y);
            } else {
                // we are in the background
                isInsideSnake = false;
            }

            KPoint2D ptNearestEdge = flowField.perimeter.nearestPoint(ptCurrent);
            if (ptNearestEdge != null) {
                // compute the outward direction to the nearest edge
                KPoint2D vToEdge = KPoint2D.newNormOf(KPoint2D.newDiffOf(ptNearestEdge, ptCurrent));
                if (!isInsideSnake) {
                    // the pixel is actually outside the points, so flip the normal direction
                    vToEdge.neg();
                }
                if (showEdgeDirection) {
                    edgePath = this.createLine(ptCurrentOFFSET, KPoint2D.newSumOf(ptCurrentOFFSET, ptNearestEdge));
                }
                KPoint2D vLateral = KPoint2D.newPerpOf(vToEdge);
                // calculate protrusion, retraction, sideways scores
                double inv_length = 1.0 / vDir.length();
                double dotToEdge = vDir.dot(vToEdge);
                double dotLateral = Math.abs(vDir.dot(vLateral));
                if (dotToEdge > 0) {
                    flowField.totalAnterogradeFlow += dotToEdge;
                    flowField.numAtereogradeFlows++;
                } else {
                    flowField.totalRetrogradeFlow += -dotToEdge;
                }
                flowField.totalLateralFlow += dotLateral;
                flowField.numDirectedFlows++;

                // Calculate hue
                //float r = (float) ((dotToEdge > 0) ? 1.0f : 0);
                //float g = (float) ((dotToEdge < 0) ? 1.0f : 0);
                //float b = (float) (dotLateral * inv_length);
                float r = (float) ((dotToEdge > 0) ? dotToEdge * inv_length : 0);
                float g = (float) ((dotToEdge < 0) ? -dotToEdge * inv_length : 0);
                float b = (float) (dotLateral * inv_length);
                shiftColor = new Color(r, g, b);
            } else {
                if (flowPoint.quality.contains(FlowVector.Quality.Significant)) {
                    shiftColor = Color.BLUE;
                }
                if (flowPoint.quality.contains(FlowVector.Quality.Significant) && flowPoint.quality.contains(FlowVector.Quality.Reliable)) {
                    shiftColor = Color.YELLOW;
                }
            }
            if (isDiagPoint) {
                // draw the template size
                overlay.addShape(frame, ptCurrentOFFSET.toRectangleShape(flowPoint.templateSize, flowPoint.templateSize), hairStroke, Color.CYAN, null);
            }
            if (flowPoint.quality.contains(FlowVector.Quality.Trackable)) {
                if (edgePath != null) {
                    overlay.addShape(frame, edgePath, hairStroke, colorNearestEdge, null);
                }
                if (vDir.length() < this.searchStepMin/2) {
                    overlay.addShape(frame, ptCurrentOFFSET.toCircleShape(0.3), null, null, Color.YELLOW);
                } else {
                    overlay.addShape(frame, quiverPath, thinStroke, shiftColor, null);
                }
            } else {
                //Color colorUntrackable = (ptCellCentroid == null) ? Color.RED.darker() : Color.DARK_GRAY;
                //overlay.addShape(frame0, ptCurrentOFFSET.toRectangleShape(0.3, 0.3), thinStroke, null, colorUntrackable);
            }
        }
        if (flowField.center != null) {
            KPoint2D ptCentroidOFFSET = KPoint2D.newSumOf(flowField.center, ptHALF_PIXEL);
            overlay.addShape(frame, ptCentroidOFFSET.toCircleShape(searchDiameterMax / 2), thinStroke, Color.YELLOW, null);
        }
    }

    public FlowField[] calculateFlow(ImagePlus imp, Snake[] snakes) {
        final int imageWidth = imp.getWidth();
        final int imageHeight = imp.getHeight();
        final int nFrames = imp.getStackSize();

        // count the number of precomputed snakes
        int nSnakes = 0;
        for (int i = 0; i < snakes.length; i++) {
            if (snakes[i] != null) {
                nSnakes++;
            }
        }

        //**
        //** Initialize flowFields data with the perimeters
        //**
        final FlowField[] flowFields = new FlowField[nFrames];
        for (int i = 0; i < nFrames; i++) {
            if (i < nSnakes) {
                flowFields[i] = new FlowField(snakes[i]);
            }
            if (i + 1 < nSnakes) {
                flowFields[i].calcPerimeterValues(snakes[i + 1]);
            }
        }

        int nFramesMax = nFrames - timeWindow;
        final int nFramesToProcess = debugTotalFrames > 0 ? Math.min(nFramesMax, debugTotalFrames) : nFramesMax;

        final ImageStack stack = imp.getStack();
        final AtomicInteger aiNextFrame = new AtomicInteger(1);
        final AtomicInteger aiNumFinished = new AtomicInteger(0);

        final int numThreads = Runtime.getRuntime().availableProcessors();
        Thread[] threads = new Thread[numThreads];

        for (int iThread = 0; iThread < numThreads; iThread++) {
            final int threadNum = iThread;
            threads[iThread] = new Thread() {

                public void run() {
                    //**
                    //** SEARCH TEMPLATE
                    //**
                    // Precompute several templates.
                    int templateHalfSizeMin = templateSizeMin / 2;
                    int templateHalfSizeMax = templateSizeMax / 2;

                    // Create a series of larger and larger square templates,
                    // each 50% bigger than the last. Also, create a series of slightly
                    // larger templates to look for variation in displacement. These
                    // templates are 25% larger than their counterparts

                    List<Template> templates = new ArrayList<Template>();
                    List<Template> templatesBigger = new ArrayList<Template>();
                    int tsize = templateHalfSizeMin;
                    int tsizeOld = tsize;
                    int tsizeBigger;
                    while (tsize <= templateHalfSizeMax) {
                        templates.add(new SquareTemplate(tsize));
                        tsizeBigger = (int) Math.round(tsize * TEMPLATE_SIZE_VARIATION);
                        if (tsizeBigger == tsize) {
                            tsizeBigger++;
                        }
                        templatesBigger.add(new SquareTemplate(tsizeBigger));
                        tsize = (int) Math.round(tsize * AUTOMATIC_TEMPLATE_SIZE_INC);
                        if (tsize == tsizeOld) {
                            tsize += 1;
                        }
                        tsizeOld = tsize;
                    }

                    Template biggestTemplate = templatesBigger.get(templatesBigger.size() - 1);
                    int nMaxPixels = biggestTemplate.numPoints() * timeWindow;

                    float[] frame0Pixels = new float[nMaxPixels];
                    float[] frame1Pixels = new float[nMaxPixels];

                    //**
                    //** SEARCH AREA
                    //**
                    // setup a series of search matricies to hold correlations in the search space
                    double searchRadiusMin = searchDiameterMin / 2;
                    double searchRadiusMax = searchDiameterMax / 2;
                    double stepSize = searchStepMin;
                    List<SearchArea> searchAreas = new ArrayList<SearchArea>((int) (searchRadiusMax - searchRadiusMin - 1));
                    for (double radius = searchRadiusMin; radius <= searchRadiusMax + epsilon; radius++) {
                        searchAreas.add(new SearchArea(radius, stepSize, CIRCULAR_SEARCH_AREA));
                        stepSize *= AUTOMATIC_SEARCH_STEP_INC;
                        if (stepSize > searchStepMax) {
                            stepSize = searchStepMax;
                        }
                    }

                    //**
                    //** SEARCH RESULTS
                    //**
                    CorrelationData[] fits = new CorrelationData[2];
                    fits[0] = new CorrelationData();
                    fits[1] = new CorrelationData();

                    for (int frame0 = aiNextFrame.getAndIncrement(); frame0 <= nFramesToProcess; frame0 = aiNextFrame.getAndIncrement()) {
                        //IJ.log(">> Starting frame " + frame0 + " thread " + threadNum + "    aiSlice="+aiNextFrame.get());
                        FlowField currentField = flowFields[frame0 - 1];

                        // pre-calculate an image mask from the points
                        ByteProcessor mask = currentField.perimeter.toMask(imageWidth, imageHeight, true);

                        //IJ.showProgress((double) frame0 / (nFramesToProcess + 1));

                        //**
                        //** STEP THROUGH EACH PIXEL OF FRAME
                        //**
                        for (int yCurrent = 0; yCurrent < imageHeight; yCurrent += flowSampleSpacing) {
                            if (frame0 == aiNextFrame.get() - aiNumFinished.get() - 1) {
                                // only show status for the furthest thread
                                IJ.showStatus("Correlating frame " + frame0 + "/" + nFramesToProcess);
                                double progress = yCurrent + 1;
                                progress /= imageHeight;
                                IJ.showProgress(progress);
                            }
                            for (int xCurrent = 0; xCurrent < imageWidth; xCurrent += flowSampleSpacing) {
                                // Determine if we are inside or on the boundary of the points outline
                                int maskVal = mask.get(xCurrent, yCurrent);
                                boolean isInsideOrEdge = maskVal > 0;

                                if (isInsideOrEdge) {
                                    CorrelationData bestFit = null;
                                    int lastTemplateWidth = 0;

                                    for (int templateSize = 0; templateSize < templates.size(); templateSize++) {
                                        for (int variation = 0; variation < 2; variation++) {
                                            Template currentTemplate;
                                            if (variation == 0) {
                                                currentTemplate = templates.get(templateSize);
                                                lastTemplateWidth = currentTemplate.width;
                                            } else {
                                                currentTemplate = templatesBigger.get(templateSize);
                                            }
                                            CorrelationData fit = fits[variation];
                                            fit.clear();
                                            fit.template = currentTemplate;

                                            int nPixels = currentTemplate.numPoints() * timeWindow;

                                            // First, gather all of the data for the current pixel stack
                                            // shift the current template to position (xCurrent,yCurrent)
                                            currentTemplate.shiftTo(xCurrent, yCurrent, imageWidth, imageHeight);
                                            gatherMultiFramePixels(stack, currentTemplate, frame0, frame0 + timeWindow - 1, frame0Pixels);

                                            double sumCurNext, sumCurSq, sumNextSq;
                                            float cur, next;

                                            // precalculate the sumCurSq data;
                                            sumCurSq = 0;
                                            for (int i = 0; i < nPixels; i++) {
                                                cur = frame0Pixels[i];
                                                sumCurSq += cur * cur;
                                            }

                                            //**
                                            //** ESTIMATE CORRELATION
                                            //**
                                            // Now, shift the template for the subsequent frame and gather the correlations for each shift point
                                            SearchArea lastArea = null;


                                            // search through all of the points until we have a reliable score
                                            for (SearchArea currentArea : searchAreas) {
                                                currentArea.clearAllScores();
                                                // copy the correlation scores from the last search
                                                if (lastArea != null) {
                                                    // only copy if they had the same step size
                                                    if (currentArea.getStepSize() == lastArea.getStepSize()) {
                                                        currentArea.copyScoresFrom(lastArea);
                                                    }
                                                }
                                                lastArea = currentArea;

                                                for (SearchPoint search : currentArea.points) {
                                                    // only recompute the correlation score if it was not computed in the last pass
                                                    if (search != null && !search.hasScore()) {

                                                        // shift the template
                                                        currentTemplate.shiftTo(xCurrent + search.offset.x, yCurrent + search.offset.y, imageWidth, imageHeight);

                                                        // gather the pixels from the shifted template
                                                        gatherMultiFramePixels(stack, currentTemplate, frame0 + 1, frame0 + timeWindow, frame1Pixels);

                                                        // determine the correlation score
                                                        sumCurNext = 0;
                                                        sumNextSq = 0;
                                                        for (int i = 0; i < nPixels; i++) {
                                                            cur = frame0Pixels[i];
                                                            next = frame1Pixels[i];
                                                            sumCurNext += cur * next;
                                                            sumNextSq += next * next;
                                                        } // END for index

                                                        // calculate the correlation score
                                                        search.score = sumCurNext / (Math.sqrt(sumCurSq) * Math.sqrt(sumNextSq));
                                                    } // END if (!search.hasScore())
                                                } // END for search

                                                // FIND THE GLOBAL MAX
                                                int[] globalMaxCoords = new int[2];
                                                fit.globalMax = currentArea.maxScore(globalMaxCoords);
                                                fit.displacementOfMax = Math.sqrt(fit.globalMax.radiusSq);
                                                fit.xIndexOfMax = globalMaxCoords[0];
                                                fit.yIndexOfMax = globalMaxCoords[1];

                                                // UNIQUENESS TEST
                                                fit.isUnique = true;
                                                for (SearchPoint pt : currentArea.points) {
                                                    if (pt != null && pt != fit.globalMax) {
                                                        if (Math.abs(fit.globalMax.score - pt.score) < epsilon) {
                                                            fit.isUnique = false;
                                                        }
                                                    }
                                                }

                                                if (!fit.isUnique) {
                                                    // Failed the uniqueness test. Stop searching
                                                    break;
                                                }

                                                // SIGNIFICANCE TEST
                                                fit.isSignificant = true;
                                                double significanceRadius = Math.max(significanceRadiusMax, 0.5 * Math.sqrt(fit.globalMax.radiusSq));
                                                double avg = currentArea.averageScoreAround(fit.globalMax.offset, significanceRadius);

                                                // Go through every point and look for
                                                // local maxima that violate the significance test

                                                // make sure to test within 1 pixel distance for a local max
                                                int localMaxTestRadius = (int) Math.round(1.0 / currentArea.getStepSize());

                                                // precompute
                                                double inv_globalmax_minus_avg = 1.0 / (fit.globalMax.score - avg);

                                                for (int ystep = 0; ystep < currentArea.getStepsHigh(); ystep++) {
                                                    for (int xstep = 0; xstep < currentArea.getStepsWide(); xstep++) {
                                                        SearchPoint sp = currentArea.getPoint(xstep, ystep);
                                                        if (sp != null && sp == fit.globalMax) {
                                                            // don't test the global maximum
                                                            break;
                                                        }
                                                        if (currentArea.isLocalMaxScore(xstep, ystep, localMaxTestRadius, 0)) {
                                                            // This is a local maximum. Test for rejection
                                                            double gamma = (sp.score - avg) * inv_globalmax_minus_avg;
                                                            if (gamma >= significanceGamma) {
                                                                // the local maximum is a significant fraction of
                                                                // the global maximum. Reject the point
                                                                fit.isSignificant = false;
                                                                break; // exit iC
                                                            }
                                                        }
                                                    } // END for xstep
                                                    if (!fit.isSignificant) {
                                                        break;
                                                    }
                                                } // END for ystep

                                                if (!fit.isSignificant) {
                                                    // Failed the significance test. Stop searching
                                                    break;
                                                }

                                                // RELIABILITY TEST
                                                double reliableRadiusSq = currentArea.getActualSearchRadius() / 2.0;
                                                reliableRadiusSq = reliableRadiusSq * reliableRadiusSq;

                                                if (fit.globalMax.radiusSq > reliableRadiusSq) {
                                                    fit.isReliable = false;
                                                } else {
                                                    fit.isReliable = true;
                                                }

                                                if (fit.isReliable) {
                                                    // passed the reliability test. Stop searching
                                                    break;
                                                }

                                            } // END for currentArea
                                        } // END for variation

                                        bestFit = fits[0];

                                        // DETERMINE IF DISPLACEMENT DID NOT VARY MUCH
                                        double displacement = fits[0].displacementOfMax;
                                        double displacementVar = fits[1].displacementOfMax;
                                        double displacementError = Math.abs(displacementVar - displacement) / significanceRadiusMax;
                                        if (bestFit.isUnique
                                                && bestFit.isReliable
                                                && bestFit.isSignificant
                                                && displacementError <= MAX_DISPLACEMENT_ERROR) {

                                            // Mark this point as trackable
                                            bestFit.isTrackable = true;
                                            // We do not need to further increase the template size
                                            break;
                                        }
                                    } // END for templateSize

                                    // if the max template size was reached, then isTrackable is automatically false

                                    if (bestFit.isUnique) {
                                        // convert the global max shift to FlowVector
                                        KPoint2D vDir = new KPoint2D(bestFit.globalMax.offset);
                                        EnumSet<FlowVector.Quality> quality = EnumSet.noneOf(FlowVector.Quality.class);
                                        quality.add(FlowVector.Quality.Unique);
                                        if (bestFit.isSignificant) {
                                            quality.add(FlowVector.Quality.Significant);
                                        }
                                        if (bestFit.isReliable) {
                                            quality.add(FlowVector.Quality.Reliable);
                                        }
                                        if (bestFit.isTrackable) {
                                            quality.add(FlowVector.Quality.Trackable);
                                        }

                                        if (bestFit.isTrackable) {
                                            currentField.addVector(xCurrent, yCurrent, vDir.x, vDir.y, quality, lastTemplateWidth);
                                        } else {
                                            //Color colorUntrackable = (ptCellCentroid == null) ? Color.RED.darker() : Color.DARK_GRAY;
                                            //overlay.addShape(frame0, ptCurrentOFFSET.toRectangleShape(0.3, 0.3), thinStroke, null, colorUntrackable);
                                        }

                                    } // END if isUnique
                                } // END if (!isBackground)

                            } // END for xCurrent
                        } // END for yCurrent
                        //IJ.log(" <<Finished frame " + frame0 + " thread " + threadNum + "   aiSlice="+aiNextFrame.get());
                    } // end for frame0
                    aiNumFinished.getAndIncrement();
                } // END run()
            }; // END new Thread()
        } // END for (iThread...)

        for (int iThread = 0; iThread < numThreads; iThread++) {
            threads[iThread].setPriority(Thread.NORM_PRIORITY);
            threads[iThread].start();
        }

        try {
            for (int iThread = 0; iThread < numThreads; iThread++) {
                threads[iThread].join();
            }
        } catch (InterruptedException ie) {
            throw new RuntimeException(ie);
        }

        IJ.showStatus("");
        IJ.showProgress(1.0);
        showAllFlows(imp, flowFields);
        return flowFields;

    }


    public void showFlowResults(FlowField[] flowFields) {
        ResultsTable rt = ResultsTable.getResultsTable();

        rt.reset();
        for (int i = 0; i < flowFields.length; i++) {
            int frame = i + 1;
            FlowField flow = flowFields[i];
            if (flow != null) {
                rt.incrementCounter();
                rt.addValue("Frame", frame);
                rt.addValue("CenterX", flow.center.x);
                rt.addValue("CenterY", flow.center.y);
                rt.addValue("PerimLength", flow.perimeterLength);
                rt.addValue("TotalArea", flow.area);
                rt.addValue("ProtrArea", flow.protrusionArea);
                rt.addValue("RetrArea", flow.retractionArea);
                rt.addValue("TotalFlow", flow.totalFlow);
                rt.addValue("NumFlows", flow.numFlows);
                rt.addValue("AnteroFlow", flow.totalAnterogradeFlow);
                rt.addValue("RetroFlow", flow.totalRetrogradeFlow);
                rt.addValue("LateralFlow", flow.totalLateralFlow);
                rt.addValue("NumDirFlows", flow.numDirectedFlows);
                rt.addValue("NumAntFlows", flow.numAtereogradeFlows);
            }
        }
        rt.show("Results");
    }

    public static GeneralPath createArrow(KPoint2D pStart, KPoint2D pEnd,
            double dLength, double dWidth) {
        // create an arrow for the end of the line

        KPolygon2D poly = new KPolygon2D(3, false);
        KPoint2D vDir = KPoint2D.newDiffOf(pStart, pEnd);
        vDir.norm();
        KPoint2D vPerp = KPoint2D.newPerpOf(vDir);
        KPoint2D pBase = KPoint2D.newSumOf(KPoint2D.newProdOf(dLength, vDir), pEnd);
        KPoint2D pWing1 = KPoint2D.newSumOf(KPoint2D.newProdOf(dWidth / 2, vPerp), pBase);
        poly.add(pStart);
        poly.add(pEnd);
        poly.add(pWing1);
        return poly.toGeneralPath();
    }

    public static GeneralPath createLine(KPoint2D pStart, KPoint2D pEnd) {
        KPoint2D[] points = {pStart, pEnd};
        return KPolygon2D.wrap(Arrays.asList(points), false).toGeneralPath();
    }

    public static void createOverlayedImage(ImagePlus imp) {
        if (!OverlayRoi.hasOverlayRoi(imp)) {
            return;
        }
        OverlayRoi overlay = (OverlayRoi) imp.getRoi();

        int iStartFrame = 1;
        int iEndFrame = imp.getStackSize();

        ImagePlus impNew = overlay.convertToImagePlus(imp.getTitle() + " Overlay", iStartFrame, iEndFrame, true);
        if (impNew != null) {
            impNew.show();
        }
    }
}
