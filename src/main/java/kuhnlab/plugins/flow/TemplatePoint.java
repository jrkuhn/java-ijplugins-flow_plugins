/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kuhnlab.plugins.flow;

/**
 * Used to precalculate template point data for pixel interpolation.
 *
 * @author jrkuhn
 */
public class TemplatePoint {

    public int xoff, yoff;                  // x, y positions of template centered at (0,0)
    public int sx0, sy0;                    // upper-left x, y positions of shifted template
    public int is00, is10, is01, is11;      // indicies into pixel array for the four corner points
    public double sxfrac, syfrac;           // fraction between shifted pixels for interpolation

    private static boolean ZERO_EDGE = true;  // replicate edges or set outside pixels to pixel value at (0,0)

    /** Creates a new template point at the specified integer position.
     *
     * @param x     pixel column of this template point
     * @param y     pixel row of this template point
     */
    public TemplatePoint(int x, int y) {
        xoff = x;
        yoff = y;
        sx0 = sy0 = 0;
        is00 = is10 = is01 = is11 = 0;
        sxfrac = syfrac = 0;
    }

    /** Copy constructor. */
    public TemplatePoint(TemplatePoint ref) {
        xoff = ref.xoff;
        yoff = ref.yoff;
        sx0 = ref.sx0;
        sy0 = ref.sy0;
        is00 = ref.is00;
        is10 = ref.is10;
        is01 = ref.is01;
        is11 = ref.is11;
        sxfrac = ref.sxfrac;
        syfrac = ref.syfrac;
    }

    /** Shifts the template to a new center and precalculates the pixel
     *  interpolation factors.
     * @param sx        new sub-pixel x position of template center
     * @param sy        new sub-pixel y position of template center
     * @param width     width of image the template will be used on
     * @param height    height of image the template will be used on
     */
    public void setShift(double sx, double sy, int width, int height) {
        sxfrac = xoff + sx;
        sx0 = (int) sxfrac;
        if (sx0 < 0) {
            // deal properly with negative numbers
            // e.g. if x=-3.2 then x0=-4, xfrac=0.8
            sx0--;
        }
        sxfrac = sxfrac - sx0;

        syfrac = yoff + sy;
        sy0 = (int) syfrac;
        if (sy0 < 0) {
            // deal properly with negative numbers
            // e.g. if y=-3.2 then y0=-4, yfrac=0.8
            sy0--;
        }
        syfrac = syfrac - sy0;
        adjustRange(width, height, true);
    }

    /** Shifts the template to a new center and precalculates the pixel
     *  interpolation factors. This version is for non-interpolated images.
     * @param isx       new column of template center
     * @param isy       new row of template center
     * @param width     width of image the template will be used on
     * @param height    height of image the template will be used on
     */
    public void setShift(int isx, int isy, int width, int height) {
        sx0 = xoff + isx;
        sxfrac = 0;
        sy0 = yoff + isy;
        syfrac = 0;
        adjustRange(width, height, false);
    }

    /** Make sure this template point fits within specified image width
     *  and height. Precomputes the 4 corner pixel positions and
     *  offsets into the pixel storage array.
     * @param width     width of image the template will be used on
     * @param height    height of image the template will be used on
     */
    protected void adjustRange(int width, int height, boolean needsInterpolation) {

        int sx1=0, sy1=0;
        if (ZERO_EDGE) {
            if (needsInterpolation) {
                sx1 = sx0 + 1;
                sy1 = sy0 + 1;
                if (sx0 < 0 || sy0 < 0 || sx0 >= width || sy0 >= height) {
                    sx0 = sy0 = 0;
                }
                if (sx1 < 0 || sy1 < 0 || sx1 >= width || sy1 >= height) {
                    sx1 = sy1 = 0;
                }
            } else {
                if (sx0 < 0 || sy0 < 0 || sx0 >= width || sy0 >= height) {
                    sx0 = sy0 = 0;
                }
            }
        } else {
            if (sx0 < 0) {
                sx0 = 0;
            }
            if (sx0 >= width) {
                sx0 = width - 1;
            }
            if (sy0 < 0) {
                sy0 = 0;
            }
            if (sy0 >= height) {
                sy0 = height - 1;
            }

            if (needsInterpolation) {
                sx1 = sx0 + 1;
                if (sx1 >= width) {
                    sx1 = width - 1;
                }

                sy1 = sy0 + 1;
                if (sy1 >= height) {
                    sy1 = height - 1;
                }
            }
        }
        // calculate the pixel array indicies for these x,y coords
        is00 = sx0 + sy0 * width;
        if (needsInterpolation) {
            is10 = sx1 + sy0 * width;
            is01 = sx0 + sy1 * width;
            is11 = sx1 + sy1 * width;
        }
    }
}
