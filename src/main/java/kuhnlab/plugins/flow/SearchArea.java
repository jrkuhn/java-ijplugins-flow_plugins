/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kuhnlab.plugins.flow;

import kuhnlab.coordinates.KPoint2D;

/**
 * Defines the search points for a rectangular 2D search grid.
 * The x,y indexes of a search point are also used to define
 * its nearest neighbors.
 *
 * @author jrkuhn
 */
public class SearchArea {

    /** Matrix of search points stored as 2D array.
     *  Holds {@code stepsWide*stepsHigh} points. */
    public SearchPoint[] points;
    /** Real radius of this search. */
    public double actualSearchRadius;
    /** Distance (in sub-pixels) between each step */
    public double stepSize;
    /** How many steps in the x direction for this area */
    public int stepsWide;
    /** How many steps in the y direction for this area */
    public int stepsHigh;

    /** Constructs a new rectangular search area. The size of the search
     * area may not be exactly determined by the requestedSearchRadius.
     * To get the actual search parameters, use the getXX functions.
     * For example, if the user requests a search distance of 3 pixels with
     * a step size of 0.5 pixels, the search will go from x,y &gt;= (-3,3) to
     * x,y &lt;= (+3,+3) with 0.5 pixel steps between each point. Note that there
     * is always a pixel at the center, so the number of search steps in
     * x and y will always be an odd number.
     *
     * @param requestedSearchRadius requested radius (in  units of pixels)
     *                              of the search area
     * @param stepSize      sub-pixel steps between search points
     * @param circular      if {@code true} the search area excludes points
     *                      outside of the radius. If {@code false}, the
     *                      search area will be a square of pixels
     */
    public SearchArea(double requestedSearchRadius, double stepSize, boolean circular) {
        this.stepSize = stepSize;
        int halfSteps = (int) Math.round(requestedSearchRadius / stepSize);
        stepsWide = stepsHigh = 2 * halfSteps + 1;
        actualSearchRadius = halfSteps * stepSize;

        points = new SearchPoint[stepsWide * stepsHigh];

        int index = 0;
        SearchPoint sp;
        double maxRadiusSq = actualSearchRadius; // + stepSize/2;
        maxRadiusSq = maxRadiusSq * maxRadiusSq;
        for (int y = -halfSteps; y <= halfSteps; y++) {
            for (int x = -halfSteps; x <= halfSteps; x++) {
                sp = new SearchPoint(x * stepSize, y * stepSize);
                if (circular && sp.radiusSq > maxRadiusSq) {
                    // Do not include points outside of the circular radius
                    sp = null;
                }
                points[index++] = sp;
            }
        }
    }

    /** Get the actual side length of this search (in units of pixels).
     *
     * @return 1/2 of the side length of this search.
     */
    public double getActualSearchRadius() {
        return actualSearchRadius;
    }

    /** Get the number of search steps in the x direction
     */
    public int getStepsWide() {
        return stepsWide;
    }

    /** Get the number of search steps in the y direction
     */
    public int getStepsHigh() {
        return stepsHigh;
    }

    /** Get the distance between steps
     */
    public double getStepSize() {
        return stepSize;
    }

    /** Get a search point at a particular x and y step
     */
    public SearchPoint getPoint(int xstep, int ystep) {
        return points[xstep + stepsWide * ystep];
    }

    /** Determine the step with the maximum correlation score
     *
     * @param coordsOfMaxOut    OPTIONAL: a 2 element array that will be
     *                          replaced with the x and y indexes of the
     *                          search point with the maximum correlation
     *                          score. Ignored if null.
     * @return                  Reference to the point with the maximum score
     */
    public SearchPoint maxScore(int[] coordsOfMaxOut) {
        double maxScore = Double.NEGATIVE_INFINITY;
        SearchPoint point, maxPoint = null;
        int xofMax = -1, yofMax = -1;
        for (int y = 0, yoff = 0; y < stepsHigh; y++, yoff += stepsWide) {
            for (int x = 0; x < stepsWide; x++) {
                point = points[x + yoff];
                if (point != null && point.score > maxScore) {
                    maxPoint = point;
                    maxScore = point.score;
                    xofMax = x;
                    yofMax = y;
                }
            }
        }
        if (coordsOfMaxOut != null) {
            coordsOfMaxOut[0] = xofMax;
            coordsOfMaxOut[1] = yofMax;
        }

        return maxPoint;
    }

    /** Finds the average correlation score within a certain distance of the
     *  point defined by ptCenter.
     *
     * @param ptCenter          point to search around
     * @param withinDistance    distance away from center point to search
     * @return
     */
    public double averageScoreAround(KPoint2D ptCenter, double withinDistance) {
        // TODO: Less brute-force method that just searches nearby like isLocalMax
        double withinDistanceSq = withinDistance * withinDistance;
        double distanceSq;
        double sum = 0;
        int npoints = 0;
        for (SearchPoint point : points) {
            if (point != null) {
                distanceSq = point.offset.distSqTo(ptCenter);
                if (distanceSq <= withinDistanceSq) {
                    sum += point.score;
                    npoints++;
                }
            }
        }

        if (npoints > 0) {
            return sum / npoints;
        } else {
            return 0;
        }
    }

    /** Determine if this any points around the requested point contain a
     * higher correlation score.
     *
     * @param xstep         step column index of central point to test
     * @param ystep         step row index of central point to test
     * @param stepsAround   how many steps around to search (NOT in units of pixels)
     * @param epsilon       fudge factor used in floating point comparison
     * @return              true if all points around this point have
     *                      correlation scores &lt; this point - epsilon
     */
    public boolean isLocalMaxScore(int xstep, int ystep, int stepsAround, double epsilon) {
        int xstart, xend, ystart, yend;
        xstart = xstep - stepsAround;
        if (xstart < 0) {
            xstart = 0;
        }
        xend = xstep + stepsAround;
        if (xend >= stepsWide) {
            xend = stepsWide - 1;
        }
        ystart = ystep - 1;
        if (ystart < 0) {
            ystart = 0;
        }
        yend = ystep + stepsAround;
        if (yend >= stepsHigh) {
            yend = stepsHigh - 1;
        }

        SearchPoint center = points[xstep + ystep * stepsWide];
        if (center == null) {
            return false;
        }
        SearchPoint neighbor;
        double neighborScore;
        double neighborMaxScore = Double.NEGATIVE_INFINITY;
        // find the maximum score of all of the neighbors
        for (int y = ystart, yoff = ystart * stepsWide; y <= yend; y++, yoff += stepsWide) {
            for (int x = xstart; x <= xend; x++) {
                neighbor = points[x + yoff];
                if (neighbor != null && neighbor != center) {
                    neighborScore = neighbor.score;
                    if (neighborScore > neighborMaxScore) {
                        neighborMaxScore = neighborScore;
                    }
                }
            }
        }

        if ((center.score - epsilon) > (neighborMaxScore)) {
            return true;
        } else {
            return false;
        }
    }

    /** Removes all score values from this search area. */
    public void clearAllScores() {
        for (SearchPoint sp : points) {
            if (sp != null) {
                sp.clearScore();
            }
        }
    }

    /** Copies previous scores from another search area.
     *  Can handle bigger or smaller source areas.
     *  Use {@code clearAllScores} first to mark scores
     *  outside of the source search area as uncalculated.
     *
     * @param src   SearchArea to copy scores from
     */
    public void copyScoresFrom(SearchArea src) {
        int widthToCopy, xstartThis, xstartSrc;
        if (this.stepsWide < src.stepsWide) {
            widthToCopy = this.stepsWide;
            xstartThis = 0;
            xstartSrc = (src.stepsWide - this.stepsWide) / 2;
        } else if (this.stepsWide == src.stepsWide) {
            widthToCopy = this.stepsWide;
            xstartThis = 0;
            xstartSrc = 0;
        } else {  // this.stepsWide > src.stepsWide
            widthToCopy = src.stepsWide;
            xstartThis = (this.stepsWide - src.stepsWide) / 2;
            xstartSrc = 0;
        }
        int heightToCopy, ystartThis, ystartSrc;
        if (this.stepsHigh < src.stepsHigh) {
            heightToCopy = this.stepsHigh;
            ystartThis = 0;
            ystartSrc = (src.stepsHigh - this.stepsHigh) / 2;
        } else if (this.stepsHigh == src.stepsHigh) {
            heightToCopy = this.stepsHigh;
            ystartThis = 0;
            ystartSrc = 0;
        } else {  // this.stepsHigh > src.stepsHigh
            heightToCopy = src.stepsHigh;
            ystartThis = (this.stepsHigh - src.stepsHigh) / 2;
            ystartSrc = 0;
        }
        int xthis, xsrc, yoffThis, yoffSrc;
        SearchPoint thisp, srcp;
        for (int y = 0; y < heightToCopy; y++) {
            yoffThis = (ystartThis + y) * this.stepsWide;
            yoffSrc = (ystartSrc + y) * src.stepsWide;
            for (int x = 0; x < widthToCopy; x++) {
                xthis = xstartThis + x;
                xsrc = xstartSrc + x;
                thisp = this.points[xthis + yoffThis];
                srcp = src.points[xsrc + yoffSrc];
                if (thisp != null && srcp != null) {
                    thisp.score = srcp.score;
                }
            }
        }

    }
}
