/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package kuhnlab.plugins.flow;

import ij.process.ByteProcessor;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;
import ij.process.ShortProcessor;
import java.awt.Shape;
import kuhnlab.coordinates.KPoint2D;

/**
 *
 * @author jrkuhn
 */
public class Template {
    /** Matrix of points in this template (as 2D array).  */
    public TemplatePoint[] points;
    /** Full width of the template */
    public int width;
    /** Full height of the template */
    public int height;
    /** should interpolation be used for gatherPixels */
    protected boolean interpolate;

    protected static final double epsilon = 1e-4;

    /** Creates a new template of size (2*halfWidth+1) x (2*halfWidth+1).
     *  The template points are centered at (0,0), x values range from
     *  -halfWidth to +halfWidth, y values range from -halfHeight to
     *  +halfHeight. NOTE: Templates will always have odd widths and heights.
     *
     * @param halfWidth     1/2 width of template.
     * @param halfHeight
     */
    public Template(int halfWidth, int halfHeight) {
        // calculate the width and height from the half width and height
        width = 2*halfWidth + 1;
        height = 2*halfHeight + 1;
        // We need to store an extra row and column of pixel data
        // for interpolation

        points = new TemplatePoint[width*height];

        int index = 0;
        for (int y = -halfHeight; y <= halfHeight; y++) {
            for (int x = -halfWidth; x <= halfWidth; x++) {
                points[index] = new TemplatePoint(x, y);
                index++;
            }
        }
        interpolate = false;
    }

    /** Creates an deep copy of a reference template */
    public Template(Template ref) {
        width = ref.width;
        height = ref.height;
        interpolate = ref.interpolate;
        points = new TemplatePoint[ref.points.length];
        for (int i=0; i<points.length; i++) {
            points[i] = new TemplatePoint(ref.points[i]);
        }
    }


    /** Returns the number of points in this template. */
    public int numPoints() {
        return points.length;
    }

    /** Should pixels be interpolated when applying this template? */
    public boolean mustInterpolate() {
        return interpolate;
    }

    /** Shifts the template to a new integer position. Use this if subpixel
     *  interpolation is not required.
     */
    public void shiftTo(int isx, int isy, int imageWidth, int imageHeight) {
        for (TemplatePoint tp : points) {
            tp.setShift(isx, isy, imageWidth, imageHeight);
        }
        interpolate = false;
    }


    /** Shifts the template to a new floating point position. Use this if subpixel
     *  interpolation is required.
     */
    public void shiftTo(double sx, double sy, int imageWidth, int imageHeight) {
        int isx = (int)sx;
        int isy = (int)sy;
        if (Math.abs(sx - isx) < epsilon && Math.abs(sy - isy) < epsilon) {
            // the shift is near an integer pixel coordinate, use the integer
            // version of shiftTo (which turns off interpolation)
            shiftTo(isx, isy, imageWidth, imageHeight);
        } else {
            for (TemplatePoint tp : points) {
                tp.setShift(sx, sy, imageWidth, imageHeight);
            }
            interpolate = true;
        }
    }
    
    /** Given a template, gathers interpolated pixel data from an 8-bit image.
     *  NOTE: Template MUST be shifted to its current position before calling.
     *
     * @param src           image to apply template to
     * @param template      shifted template used to gather pixels
     * @param dest          array to place pixels in
     * @param destPos       starting index for placing pixels
     * @param length        number of pixels to gather (usually same as template size.
     */
    public void gatherPixels(ByteProcessor src, float[] dest, int destPos, int length) {
        byte[] pixels = (byte[])src.getPixels();
        int pix00, pix10, pix01, pix11;
        double xinterp0, xinterp1;
        TemplatePoint pt;
        if (mustInterpolate()) {
            for (int i=0, idest=destPos; i < length; i++, idest++) {
                pt = points[i];
                pix00 = pixels[pt.is00] & 0xff;
                pix10 = pixels[pt.is10] & 0xff;
                pix01 = pixels[pt.is01] & 0xff;
                pix11 = pixels[pt.is11] & 0xff;

                xinterp0 = (pix00 + pt.sxfrac * (pix10 - pix00));
                xinterp1 = (pix01 + pt.sxfrac * (pix11 - pix01));

                dest[idest] = (float)(xinterp0 + pt.syfrac * (xinterp1 - xinterp0));
            }
        } else {
            for (int i=0, idest=destPos; i < length; i++, idest++) {
                pt = points[i];
                pix00 = pixels[pt.is00] & 0xff;
                dest[idest] = (float)pix00;
            }

        }
    }

    /** Given a template, gathers interpolated pixel data from a 16-bit image.
     *  NOTE: Template MUST be shifted to its current position before calling.
     *
     * @param src           image to apply template to
     * @param template      shifted template used to gather pixels
     * @param dest          array to place pixels in
     * @param destPos       starting index for placing pixels
     * @param length        number of pixels to gather (usually same as template size.
     */
    public void gatherPixels(ShortProcessor src, float[] dest, int destPos, int length) {
        short[] pixels = (short[])src.getPixels();
        int pix00, pix10, pix01, pix11;
        double xinterp0, xinterp1;
        TemplatePoint pt;
        if (mustInterpolate()) {
            for (int i=0, idest=destPos; i < length; i++, idest++) {
                pt = points[i];
                pix00 = pixels[pt.is00] & 0xffff;
                pix10 = pixels[pt.is10] & 0xffff;
                pix01 = pixels[pt.is01] & 0xffff;
                pix11 = pixels[pt.is11] & 0xffff;

                xinterp0 = (pix00 + pt.sxfrac * (pix10 - pix00));
                xinterp1 = (pix01 + pt.sxfrac * (pix11 - pix01));

                dest[idest] = (float)(xinterp0 + pt.syfrac * (xinterp1 - xinterp0));
            }
        } else {
            for (int i=0, idest=destPos; i < length; i++, idest++) {
                pt = points[i];
                pix00 = pixels[pt.is00] & 0xffff;
                dest[idest] = (float)pix00;
            }

        }
    }

    /** Given a template, gathers interpolated pixel data from a 32-bit
     *  (floating point) image.
     *  NOTE: Template MUST be shifted to its current position before calling.
     *
     * @param src           image to apply template to
     * @param template      shifted template used to gather pixels
     * @param dest          array to place pixels in
     * @param destPos       starting index for placing pixels
     * @param length        number of pixels to gather (usually same as template size.
     */
    public void gatherPixels(FloatProcessor src, float[] dest, int destPos, int length) {
        float[] pixels = (float[])src.getPixels();
        float pix00, pix10, pix01, pix11;
        double xinterp0, xinterp1;
        TemplatePoint pt;
        if (mustInterpolate()) {
            for (int i=0, idest=destPos; i < length; i++, idest++) {
                pt = points[i];
                pix00 = pixels[pt.is00];
                pix10 = pixels[pt.is10];
                pix01 = pixels[pt.is01];
                pix11 = pixels[pt.is11];

                xinterp0 = (pix00 + pt.sxfrac * (pix10 - pix00));
                xinterp1 = (pix01 + pt.sxfrac * (pix11 - pix01));

                dest[idest] = (float)(xinterp0 + pt.syfrac * (xinterp1 - xinterp0));
            }
        } else {
            for (int i=0, idest=destPos; i < length; i++, idest++) {
                pt = points[i];
                pix00 = pixels[pt.is00];
                dest[idest] = (float)pix00;
            }

        }
    }

    /** Given a template, gathers interpolated pixel data from an 8-, 16-,
     *  or 32-bit image.
     *  NOTE: Template MUST be shifted to its current position before calling.
     *
     * @param src           image to apply template to
     * @param template      shifted template used to gather pixels
     * @param dest          array to place pixels in
     * @param destPos       starting index for placing pixels
     * @param length        number of pixels to gather (usually same as template size.
     */
    public void gatherPixels(ImageProcessor src, float[] dest, int destPos, int length) {
        if (src instanceof ByteProcessor) {
            gatherPixels((ByteProcessor)src, dest, destPos, length);
        } else if (src instanceof ShortProcessor) {
            gatherPixels((ShortProcessor)src, dest, destPos, length);
        } else if (src instanceof FloatProcessor) {
            gatherPixels((FloatProcessor)src, dest, destPos, length);
        } else {
            throw new IllegalArgumentException("Unsupported image type");
        }
    }
    
    /** Determine if gatherPixels can support this ImageProcessor type. */
    static public boolean CanHandleProcessor(ImageProcessor ip) {
        if ((ip instanceof ByteProcessor) ||
                (ip instanceof ShortProcessor) ||
                (ip instanceof FloatProcessor)) {
            return true;
        }
        return false;
    }

    /** Convert this template to a shape for drawing. */
    public Shape toShape(KPoint2D ptCenter) {
        return ptCenter.toRectangleShape(width, height);
    }

}
