/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package kuhnlab.plugins.flow;

/** Special case of the RectangularTemplate where width=height.
 *
 * @author jrkuhn
 */
public class SquareTemplate extends Template {
    /** Creates a new template of size (2*halfSize+1) in both x and y.
     *  The template points are centered at (0,0), x and y values range from
     *  -halfSize to +halfSize.
     *  NOTE: Templates will always have odd widths and heights.
     *
     * @param halfWidth     1/2 width of template.
     * @param halfHeight
     */
    public SquareTemplate(int halfSize) {
        super(halfSize, halfSize);
    }

    /** Creates an deep copy of a reference template */
    public SquareTemplate(SquareTemplate ref) {
        super(ref);
    }
}
